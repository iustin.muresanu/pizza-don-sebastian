
from django.shortcuts import render
from django.http import HttpResponse


def homepage(request):
    return render(request, 'pizza/homepage.html')

def about_us(request):
    return render(request, 'pizza/about.html')

def lista_pizza(request):
    return render(request, 'pizza/pizza.html')

def contact(request):
    return render(request, 'pizza/contact.html')

def photogallery(request):
    return render(request, 'pizza/photogallery.html')


