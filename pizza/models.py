from django.db import models

from django.contrib.auth.models import AbstractUser

class PizzaUser(AbstractUser):
    phone_number = models.CharField(max_length=20)
    address = models.CharField(max_length=50)


class Category(models.Model):
    name = models.CharField(max_length=255)


class Product(models.Model):
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    ingredients = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)


class Cart(models.Model):
    status = models.CharField(max_length=31, choices=(('open', 'Open'), ('closed', 'Closed')))
    user = models.ForeignKey(PizzaUser, on_delete=models.CASCADE)

class CartItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
