
from django.urls import path, include
from pizza import views

urlpatterns = [
    path('', views.homepage, name='home'),
    path('about', views.about_us, name='about'),
    path('lista_pizza', views.lista_pizza, name='pizza'),
    path('contact', views.contact, name='contact'),
    path('photogallery', views.photogallery, name='photogallery'),

]


